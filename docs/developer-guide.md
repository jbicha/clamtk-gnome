# Tech

## Python

The extension can run both with Python 2 and Python 3

## nautilus-python

### gitlab home

https://gitlab.gnome.org/GNOME/nautilus-python

Examples: https://gitlab.gnome.org/GNOME/nautilus-python/-/tree/master/examples

News: https://gitlab.gnome.org/GNOME/nautilus-python/-/blob/master/NEWS

### Gnome wiki page

https://wiki.gnome.org/Projects/NautilusPython

## Interface to clamtk

clamtk-gnome communicates with clamtk via the command line at the moment: `os.system('clamtk %s &' % filename)`

## Tips

### Restarting Nautilus

You can use `killall nautilus` to close nautilus instances. `kill -9 pid` may sometimes have to be used (with the pid that you get from `ps aux | grep nautilus`)

More tips can be found in these files in the nautilus-python gitlab repo:
* [README](https://gitlab.gnome.org/GNOME/nautilus-python/-/blob/master/README)
* [examples/README](https://gitlab.gnome.org/GNOME/nautilus-python/-/blob/master/examples/README)


# Social

Please keep Tord and/or Dave in the loop when updating this plugin

* tord (dot) dellsen (at) gmail (dot) com
* dave (dot) nerd (at) gmail (dot) com

Tord has a background with Python and Dave has experience with Perl
